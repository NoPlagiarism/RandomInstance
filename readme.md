# RandomInstance

A [FastAPI](https://fastapi.tiangolo.com)-based backend for [GotHub's website](https://gothub.app) to power the random instance feature.

## Setup

We use Docker for deploying this, so you can just run the following command to get it up and running:

```bash
docker run -d -p 8080:8080 --name random-instance codeberg.org/gothub/randominstance
```

## Manual

Okay, you want to do it manually? sure.

### Requirements

- [Python 3.8+](https://www.python.org/downloads/)

### Running

```bash
git clone https://codeberg.org/gothub/randominstance.git
cd randominstance
pip install -r requirements.txt
uvicorn main:app --reload
```